function handle_channel_click(url) {
  fetch(`/controls/play?url=${url}`);
}

function handle_stop() {
  fetch("/controls/stop");
}

function handle_volume(delta) {
  fetch(`/controls/volume?vol=${delta}`);
}
