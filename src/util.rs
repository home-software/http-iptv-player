use std::str::FromStr;

use lazy_static::lazy_static;
use regex::Regex;

#[derive(Clone, Debug)]
pub(crate) struct Channel {
    pub(crate) id: u32,
    pub(crate) logo: Option<String>,
    pub(crate) name: String,
    pub(crate) url: String,
}

pub(crate) fn parse_channels(s: &str) -> Vec<Channel> {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r#"tvg-id="([0-9]+)" tvg-logo="(.*?)",(.*)[\r\n]+(.*)"#).unwrap();
    }
    RE.captures_iter(s)
        .map(|cap| {
            let id = cap[1].parse::<u32>().unwrap_or(0);

            let logo = cap[2].to_string();
            let logo = if logo.is_empty() { None } else { Some(logo) };

            let name = cap[3].to_string();
            let mut url = url::Url::parse(&cap[4].to_string()).unwrap();

            let host = url.host_str().unwrap();
            let new_host = dns_lookup::lookup_addr(&std::net::IpAddr::V4(
                std::net::Ipv4Addr::from_str(host).expect("invalid ip!"),
            ));
            let new_host = new_host.unwrap_or(host.to_string());

            url.set_host(Some(&new_host)).unwrap();

            let url = url.to_string();

            Channel {
                id,
                logo,
                name,
                url,
            }
        })
        .collect()
}
