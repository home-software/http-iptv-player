mod util;

use crate::util::parse_channels;
use actix_web::{web, App, HttpResponse, HttpServer};
use lazy_static::lazy_static;
use libmpv::Mpv;
use serde::Deserialize;
use std::sync::mpsc;
use std::thread;

const M3U_URL: &str = "http://iptv.kaza.cz";
const EPG_URL: &str = "http://iptv.kaza.cz/epg";
const LOGOS_BASE_URL: &str = "http://iptv.kaza.cz/logos";

const INDEX_TEMPLATE: &str = include_str!("../static/templates/index.html");

const CHANNEL_PLACEHOLDER_IMAGE: &[u8] = include_bytes!("../static/img/channel-placeholder.png");
lazy_static! {
    static ref CHANNEL_PLACEHOLDER_IMAGE_BASE64: String = format!(
        "data:image/png;base64,{}",
        base64::encode(CHANNEL_PLACEHOLDER_IMAGE)
    );
}

enum MpvCommands {
    Play(String),
    Stop,
    ChangeVolume(i64),
    Exit,
}

#[derive(Deserialize)]
struct Play {
    url: String,
}

#[derive(Deserialize)]
struct Volume {
    vol: i64,
}

#[actix_web::main]
async fn main() {
    let m3u_text = reqwest::get(M3U_URL).await.unwrap().text().await.unwrap();

    let channels = parse_channels(&m3u_text);

    let (tx, rx) = mpsc::channel::<MpvCommands>();

    let mpv_handle = thread::spawn(move || {
        let mpv = Mpv::new().expect("failed to create mpv instance!");
        mpv.set_property("fullscreen", true).unwrap();
        mpv.set_property("volume", 50).unwrap();
        loop {
            match rx.recv().unwrap() {
                MpvCommands::Stop => {
                    mpv.command("stop", &[]).unwrap();
                }
                MpvCommands::Play(url) => {
                    mpv.command("loadfile", &[&url]).unwrap();
                }
                MpvCommands::ChangeVolume(delta) => {
                    let before_volume = mpv.get_property::<i64>("volume").unwrap();
                    mpv.set_property("volume", before_volume + delta).unwrap();
                }
                MpvCommands::Exit => break,
            };
        }
    });

    tx.send(MpvCommands::Play((&channels[0]).url.clone()))
        .unwrap();

    let tx_clone = tx.clone();
    let channels_clone = channels.clone();
    HttpServer::new(move || {
        let tx1 = tx_clone.clone();
        let tx2 = tx_clone.clone();
        let tx3 = tx_clone.clone();
        let tx4 = tx_clone.clone();
        let channels1 = channels_clone.clone();
        App::new()
            .route(
                "/controls",
                web::get().to(move || {
                    let channels = channels1
                        .iter()
                        .map(|c| {
                            format!(
                                include_str!("../static/templates/channel.html"),
                                if let Some(logo) = &c.logo {
                                    format!("{}/{}", LOGOS_BASE_URL, logo)
                                } else {
                                    CHANNEL_PLACEHOLDER_IMAGE_BASE64.to_string()
                                },
                                c.name,
                                c.url,
                                CHANNEL_PLACEHOLDER_IMAGE_BASE64.to_string(),
                            )
                        })
                        .collect::<Vec<_>>();
                    async move {
                        HttpResponse::Ok()
                            .content_type("text/html; charset=utf-8")
                            .body(format!(
                                include_str!("../static/templates/index.html"),
                                include_str!("../static/style.css"),
                                channels.join("\n"),
                                include_str!("../static/main.js")
                            ))
                    }
                }),
            )
            .route(
                "/controls/stop",
                web::get().to(move || {
                    println!("stopping...");
                    tx1.send(MpvCommands::Stop).unwrap();
                    async { "stopped!" }
                }),
            )
            .route(
                "/controls/play",
                web::get().to(move |data: web::Query<Play>| {
                    println!("playing url...");
                    tx2.send(MpvCommands::Play(data.url.clone())).unwrap();
                    async move { format!("loading: {}", data.url) }
                }),
            )
            .route(
                "/controls/volume",
                web::get().to(move |data: web::Query<Volume>| {
                    println!("adjusting volume...");
                    tx3.send(MpvCommands::ChangeVolume(data.vol)).unwrap();
                    async move { format!("changing volume: {}", data.vol) }
                }),
            )
            .route(
                "/controls/exit",
                web::get().to(move || {
                    println!("exiting...");
                    tx4.send(MpvCommands::Exit).unwrap();
                    async { "exiting!" }
                }),
            )
    })
    .bind(("::", 8080))
    .unwrap()
    .run()
    .await
    .unwrap();

    tx.send(MpvCommands::Exit).unwrap();
    mpv_handle.join().unwrap();
}
