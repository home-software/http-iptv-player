### If you're not on <https://git.kroner.dev/home-software/http-iptv-player>, then you're on a mirror.

# http-iptv-player

simple IPTV player that uses mpv and is controlled by a horrible-looking web
interface

web interface url: `0.0.0.0:8080/controls`
